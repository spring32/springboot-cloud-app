package com.eara.microservices.app.student.core.vo;

/**
 * This interface is used to render explicit
 * which classes perform the value object role.
 * To be implemented.
 *
 * @author Ernesto A. Rodriguez Acosta
 */
public interface ValueObject {
}

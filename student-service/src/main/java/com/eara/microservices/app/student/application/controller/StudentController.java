package com.eara.microservices.app.student.application.controller;

import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/students")
public class StudentController {

    @PostMapping("/")
    public String dummyAdd(@RequestBody String student) {
        return "PostMapping dummyAdd";
    }

    @GetMapping("/")
    public List<String> dummyFindAll() {
        List<String> results = new ArrayList<>();
        results.add("GetMapping dummyFindAll");
        return results;
    }

    @GetMapping("/{id}")
    public String dummyFindById(@PathVariable("id") Long id) {
        return "GetMapping dummyFindById " + id;
    }
}

package com.eara.microservices.app.student.application.exception;

public class UnsupportedStateTransitionException extends RuntimeException {

    public UnsupportedStateTransitionException(Enum state) {
        super(new StringBuilder()
                .append("current state: ")
                .append(state)
                .toString());
    }
}
